=== Construction RenovationX  ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 6.2
Requires PHP: 7.0
Stable tag: 1.0.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Construction RenovationX is a stunning, advanced and exciting free construction WordPress theme that is responsive and simple to use. With the aid of the theme's default customizer features and 10+ custom patterns, you can design & develop a variety of business websites, including those for consulting, finance, agencies, industries, education, fashion, health & medical, wedding, photography, gyms, architecture, and lawyers.

== Description ==

Construction RenovationX is a stunning, advanced and exciting free construction WordPress theme that is responsive and simple to use. With the aid of the theme's default customizer features and 10+ custom patterns, you can design & develop a variety of business websites, including those for consulting, finance, agencies, industries, education, fashion, health & medical, wedding, photography, gyms, architecture, and lawyers. Construction RenovationX theme has some exciting features like (reorder each home section), cross-browser compatible, translation ready, site speed optimized, SEO friendly theme, and also supports WooCommerce. It is also fully compatible with the newest page builders plugins (Elementor, SiteOrigin, Visual Composer). And it's include 10+ different patterns, which you can design and implement in any page or post to create, edit, and update your page design. You can consult our theme documentation or get in touch with our helpful support staff if you experience any issues with our theme.


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme demo? =
You can check our Theme Demo at https://demo.sparklewpthemes.com/constructionlight/renonvation/



= Where can I find theme all features ? =

You can check our Theme features at https://sparklewpthemes.com/wordpress-themes/construction-wordpress-theme/


== Translation ==

Construction RenovationX  theme is translation ready.


== Copyright ==

Construction RenovationX  WordPress Theme is child theme of constructionlight, Copyright 2017 Sparkle Themes.
Construction RenovationX  is distributed under the terms of the GNU GPL

Construction RenovationX  WordPress Theme, Copyright (C) 2021 Sparkle Themes.
Construction RenovationX  is distributed under the terms of the GNU GPL

== Credits ==

	Images used in screenshot

	* https://pxhere.com/en/photo/633136, License: CCO (https://pxhere.com/en/license)
	* https://pxhere.com/en/photo/605769, License: CCO (https://pxhere.com/en/license)

== Changelog ==
= 1.0.2 26th April 2023 =
 ** Pattners icon issue fix in parent theme

= 1.0.0 26th March 2023 =
 ** Initial submit theme on wordpress.org trac.