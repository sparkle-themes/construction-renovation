<?php
 /**
  * Title: Sidebar
  * Slug: construction-renovationx/sidebar
  * Categories: construction-renovationx
  * Keywords: sidebar, sections
  */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"bottom":"3rem","top":"2rem"},"blockGap":"20px"},"border":{"radius":"10px"}},"backgroundColor":"primary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-primary-background-color has-background" style="border-radius:10px;padding-top:2rem;padding-bottom:3rem"><!-- wp:heading {"textAlign":"center","level":3,"textColor":"background"} -->
<h3 class="has-text-align-center has-background-color has-text-color">BOOKING FORM </h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"spacing":{"padding":{"top":"5px","bottom":"5px"}}},"textColor":"background","fontSize":"small","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-color has-text-color has-poppins-font-family has-small-font-size" style="padding-top:5px;padding-bottom:5px">Malesuada incidunt excepturi proident quo eros? Sinterdum praesent magnis, eius cumque.</p>
<!-- /wp:paragraph -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}},"border":{"radius":"34px"},"color":{"background":"#f8f8f8"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background" style="border-radius:34px;background-color:#f8f8f8;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:heading {"textAlign":"left","level":4,"style":{"typography":{"fontStyle":"normal","fontWeight":"300"},"color":{"text":"#6d6d6d"},"spacing":{"padding":{"top":"10px","bottom":"10px","left":"15px"}}},"fontSize":"medium","fontFamily":"poppins"} -->
<h4 class="has-text-align-left has-text-color has-poppins-font-family has-medium-font-size" style="color:#6d6d6d;padding-top:10px;padding-bottom:10px;padding-left:15px;font-style:normal;font-weight:300">Your Name...</h4>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}},"border":{"radius":"34px"},"color":{"background":"#f8f8f8"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background" style="border-radius:34px;background-color:#f8f8f8;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"},"color":{"text":"#6d6d6d"},"spacing":{"padding":{"left":"15px","bottom":"10px","top":"10px"}}},"fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="has-text-align-left has-text-color has-poppins-font-family has-medium-font-size" style="color:#6d6d6d;padding-top:10px;padding-bottom:10px;padding-left:15px;font-style:normal;font-weight:300">Your Email...</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"4.5rem"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"style":{"spacing":{"padding":{"bottom":"10px"}}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-background-color has-text-color has-poppins-font-family" style="padding-bottom:10px">Appointment Date</p>
<!-- /wp:paragraph -->

<!-- wp:group {"style":{"border":{"radius":"45px","width":"1px"}},"borderColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-border-color has-background-border-color" style="border-width:1px;border-radius:45px"><!-- wp:paragraph {"style":{"spacing":{"padding":{"top":"10px","right":"10px","bottom":"10px","left":"15px"}}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-background-color has-text-color has-poppins-font-family" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:15px">MM / DD / YY</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"radius":"26px","width":"1px"}},"borderColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-border-color has-background-border-color" style="border-width:1px;border-radius:26px"><!-- wp:paragraph {"align":"center","style":{"spacing":{"padding":{"left":"15px","top":"10px","right":"10px","bottom":"10px"}}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-color has-text-color has-poppins-font-family" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:15px">INQUIRY NOW</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|40","padding":{"top":"var:preset|spacing|60","right":"var:preset|spacing|40","bottom":"var:preset|spacing|60","left":"var:preset|spacing|40"}},"border":{"width":"1px","radius":"5px"}},"borderColor":"border","className":"has-no-hover-shadow-dark animated animated-fadeInUp","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp has-border-color has-border-border-color" style="border-width:1px;border-radius:5px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--40);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--40)"><!-- wp:image {"align":"center","id":212,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":"100px","width":"2px"}},"borderColor":"primary"} -->
<figure class="wp-block-image aligncenter size-full has-custom-border"><img src="https://demo.sparkletheme.com/hello-fse/travel/wp-content/uploads/sites/2/2023/03/Adam-Szucs.jpeg" alt="" class="has-border-color has-primary-border-color wp-image-212" style="border-width:2px;border-radius:100px"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3,"style":{"spacing":{"padding":{"bottom":"1rem"}}},"fontFamily":"poppins"} -->
<h3 class="has-text-align-center has-poppins-font-family" style="padding-bottom:1rem">Brad Wegner</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","textColor":"foreground","fontFamily":"poppins"} -->
<p class="has-text-align-center has-foreground-color has-text-color has-poppins-font-family">Quaerat inventore! Vestibulum aenean volutpat gravida. Sagittis, euismod perferendis.</p>
<!-- /wp:paragraph -->

<!-- wp:social-links {"style":{"spacing":{"blockGap":{"top":"var:preset|spacing|30","left":"var:preset|spacing|30"}}},"layout":{"type":"flex","justifyContent":"center"}} -->
<ul class="wp-block-social-links"><!-- wp:social-link {"url":"#","service":"facebook"} /-->

<!-- wp:social-link {"url":"#","service":"wordpress"} /-->

<!-- wp:social-link {"url":"#","service":"amazon"} /-->

<!-- wp:social-link {"url":"#","service":"instagram"} /-->

<!-- wp:social-link {"url":"#","service":"twitter"} /--></ul>
<!-- /wp:social-links --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"style":{"spacing":{"blockGap":"5px"}},"className":"has-no-hover-shadow-dark animated animated-fadeInUp"} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"}}},"backgroundColor":"white"} -->
<div class="wp-block-group has-white-background-color has-background" style="padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:search {"label":"","showLabel":false,"placeholder":" Search","width":100,"widthUnit":"%","buttonText":"Search","buttonUseIcon":true,"style":{"border":{"radius":"0px","width":"0px","style":"none"}},"backgroundColor":"primary","textColor":"background"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"25px","right":"30px","bottom":"25px","left":"30px"},"blockGap":"0px"}},"backgroundColor":"white","className":"has-no-hover-shadow-dark animated animated-fadeInUp"} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp has-white-background-color has-background" style="padding-top:25px;padding-right:30px;padding-bottom:25px;padding-left:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"10px","margin":{"top":"0px","bottom":"0px"},"padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group" style="margin-top:0px;margin-bottom:0px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:heading {"level":3,"align":"wide","style":{"spacing":{"margin":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"textColor":"foreground","fontSize":"content-heading"} -->
<h3 class="alignwide has-foreground-color has-text-color has-content-heading-font-size" style="margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px">Categories</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","layout":{"inherit":false}} -->
<div class="wp-block-group alignwide"><!-- wp:separator {"backgroundColor":"border","className":"is-style-wide"} -->
<hr class="wp-block-separator has-text-color has-border-color has-alpha-channel-opacity has-border-background-color has-background is-style-wide"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:categories /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"25px","right":"30px","bottom":"25px","left":"30px"},"blockGap":"0px"}},"backgroundColor":"white","className":"has-no-hover-shadow-dark animated animated-fadeInUp"} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp has-white-background-color has-background" style="padding-top:25px;padding-right:30px;padding-bottom:25px;padding-left:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"10px","margin":{"top":"0px","bottom":"0px"},"padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group" style="margin-top:0px;margin-bottom:0px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:heading {"level":3,"align":"wide","style":{"spacing":{"margin":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"textColor":"foreground","fontSize":"content-heading"} -->
<h3 class="alignwide has-foreground-color has-text-color has-content-heading-font-size" style="margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px">Recent Posts</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","layout":{"inherit":false}} -->
<div class="wp-block-group alignwide"><!-- wp:separator {"backgroundColor":"border","className":"is-style-wide"} -->
<hr class="wp-block-separator has-text-color has-border-color has-alpha-channel-opacity has-border-background-color has-background is-style-wide"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:latest-posts {"displayPostDate":true,"displayFeaturedImage":true,"featuredImageAlign":"left","featuredImageSizeWidth":75,"featuredImageSizeHeight":75} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"25px","right":"30px","bottom":"25px","left":"30px"},"blockGap":"0px"}},"className":"has-no-hover-shadow-dark animated animated-fadeInUp"} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp" style="padding-top:25px;padding-right:30px;padding-bottom:25px;padding-left:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"10px","margin":{"top":"0px","bottom":"0px"},"padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group" style="margin-top:0px;margin-bottom:0px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:heading {"level":3,"align":"wide","style":{"spacing":{"margin":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"textColor":"foreground","fontSize":"content-heading"} -->
<h3 class="alignwide has-foreground-color has-text-color has-content-heading-font-size" style="margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px">Archive</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","layout":{"inherit":false}} -->
<div class="wp-block-group alignwide"><!-- wp:separator {"backgroundColor":"border","className":"is-style-wide"} -->
<hr class="wp-block-separator has-text-color has-border-color has-alpha-channel-opacity has-border-background-color has-background is-style-wide"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:archives /--></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"25px","right":"30px","bottom":"25px","left":"30px"},"blockGap":"0px"}},"className":"has-no-hover-shadow-dark animated animated-fadeInUp"} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp" style="padding-top:25px;padding-right:30px;padding-bottom:25px;padding-left:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"10px","margin":{"top":"0px","bottom":"0px"},"padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group" style="margin-top:0px;margin-bottom:0px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:heading {"level":3,"align":"wide","style":{"spacing":{"margin":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"textColor":"foreground","fontSize":"content-heading"} -->
<h3 class="alignwide has-foreground-color has-text-color has-content-heading-font-size" style="margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px">Tags</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","layout":{"inherit":false}} -->
<div class="wp-block-group alignwide"><!-- wp:separator {"backgroundColor":"border","className":"is-style-wide"} -->
<hr class="wp-block-separator has-text-color has-border-color has-alpha-channel-opacity has-border-background-color has-background is-style-wide"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:tag-cloud /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"25px","right":"30px","bottom":"25px","left":"30px"},"blockGap":"0px"}},"className":"has-no-hover-shadow-dark animated animated-fadeInUp"} -->
<div class="wp-block-group has-no-hover-shadow-dark animated animated-fadeInUp" style="padding-top:25px;padding-right:30px;padding-bottom:25px;padding-left:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"10px","margin":{"top":"0px","bottom":"0px"},"padding":{"top":"0px","right":"0px","bottom":"20px","left":"0px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group" style="margin-top:0px;margin-bottom:0px;padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px"><!-- wp:heading {"level":3,"align":"wide","style":{"spacing":{"margin":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"textColor":"foreground","fontSize":"content-heading"} -->
<h3 class="alignwide has-foreground-color has-text-color has-content-heading-font-size" style="margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px">Social Links</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","layout":{"inherit":false}} -->
<div class="wp-block-group alignwide"><!-- wp:separator {"backgroundColor":"border","className":"is-style-wide"} -->
<hr class="wp-block-separator has-text-color has-border-color has-alpha-channel-opacity has-border-background-color has-background is-style-wide"/>
<!-- /wp:separator --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-group" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:social-links {"iconColor":"background","iconColorValue":"#ffffff","openInNewTab":true,"size":"has-small-icon-size","style":{"spacing":{"blockGap":"20px"}},"className":"is-style-default","layout":{"type":"flex","justifyContent":"left"}} -->
<ul class="wp-block-social-links has-small-icon-size has-icon-color is-style-default"><!-- wp:social-link {"url":"#","service":"facebook"} /-->

<!-- wp:social-link {"url":"#","service":"twitter"} /-->

<!-- wp:social-link {"url":"#","service":"linkedin"} /-->

<!-- wp:social-link {"url":"#","service":"instagram"} /--></ul>
<!-- /wp:social-links --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->