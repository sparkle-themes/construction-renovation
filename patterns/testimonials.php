<?php
 /**
  * Title: Testimonials
  * Slug: construction-renovationx/testimonials
  * Categories: construction-renovationx
  * Keywords: Testimonial, customer voice, client says
  */
?>
<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull" id="testimonial"><!-- wp:cover {"url":"https://demo.sparkletheme.com/hello-fse/renovation/wp-content/uploads/sites/5/2023/03/home-hero-banner.jpeg","id":15,"hasParallax":true,"dimRatio":0,"minHeight":540,"minHeightUnit":"px","isDark":false,"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}}} -->
<div class="wp-block-cover alignfull is-light has-parallax" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70);min-height:540px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><div role="img" class="wp-block-cover__image-background wp-image-15 has-parallax" style="background-position:50% 50%;background-image:url(https://demo.sparkletheme.com/hello-fse/renovation/wp-content/uploads/sites/5/2023/03/home-hero-banner.jpeg)"></div><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"15px","padding":{"top":"30px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":" animated animated-fadeInUp","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":326,"width":25,"height":25,"sizeSlug":"full","linkDestination":"none","style":{"color":{"duotone":["#fff","#ffffff"]}}} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparklewpthemes.com/spiderprime/wp-content/uploads/2023/02/title_shape_2-1.png" alt="" class="wp-image-326" width="25" height="25"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"left","style":{"typography":{"letterSpacing":"1px"}},"textColor":"background","fontSize":"medium"} -->
<p class="has-text-align-left has-background-color has-text-color has-medium-font-size" style="letter-spacing:1px">Client Voice</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"lineHeight":"1.3","fontStyle":"normal","fontWeight":"500"}},"textColor":"background","className":" animated animated-fadeInUp","fontSize":"slider-title"} -->
<h2 class="has-text-align-center animated animated-fadeInUp has-background-color has-text-color has-slider-title-font-size" style="font-style:normal;font-weight:500;line-height:1.3">What Happy Clients Says</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"background","fontSize":"large"} -->
<p class="has-text-align-center has-background-color has-text-color has-large-font-size" style="font-style:normal;font-weight:600">About Us?</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"30px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="padding-top:30px;padding-bottom:30px"><!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"top":"50px"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:50px"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"style":{"spacing":{"padding":{"bottom":"15px","left":"30px"}},"border":{"width":"0px","style":"none"}},"className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark" style="border-style:none;border-width:0px;padding-bottom:15px;padding-left:30px"><!-- wp:image {"align":"left","id":332,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":"10px","width":"10px"}},"borderColor":"primary"} -->
<figure class="wp-block-image alignleft size-full has-custom-border"><img src="https://demo.sparklewpthemes.com/spiderprime/wp-content/uploads/2023/02/testi_3_4.jpg" alt="" class="has-border-color has-primary-border-color wp-image-332" style="border-width:10px;border-radius:10px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"},"blockGap":"10px"}},"backgroundColor":"white","className":"has-shadow-dark  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide has-shadow-dark animated animated-fadeInUp has-white-background-color has-background" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:30px;padding-bottom:30px"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground"} -->
<p class="has-text-align-left has-foreground-color has-text-color" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
<!-- /wp:paragraph -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textColor":"foreground","fontSize":"content-heading"} -->
<h2 class="has-foreground-color has-text-color has-content-heading-font-size">David Fernandes</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"foreground"} -->
<p class="has-foreground-color has-text-color">CEO , Founder</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"style":{"spacing":{"padding":{"bottom":"15px","left":"30px"}},"border":{"width":"0px","style":"none"}},"className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark" style="border-style:none;border-width:0px;padding-bottom:15px;padding-left:30px"><!-- wp:image {"align":"left","id":347,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":"10px","width":"10px"}},"borderColor":"primary"} -->
<figure class="wp-block-image alignleft size-full has-custom-border"><img src="https://demo.sparklewpthemes.com/spiderprime/wp-content/uploads/2023/02/testi_3_1.jpg" alt="" class="has-border-color has-primary-border-color wp-image-347" style="border-width:10px;border-radius:10px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"},"blockGap":"10px"}},"backgroundColor":"white","className":"has-shadow-dark  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide has-shadow-dark animated animated-fadeInUp has-white-background-color has-background" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:30px;padding-bottom:30px"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground"} -->
<p class="has-text-align-left has-foreground-color has-text-color" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
<!-- /wp:paragraph -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textColor":"foreground","fontSize":"content-heading"} -->
<h2 class="has-foreground-color has-text-color has-content-heading-font-size">David Fernandes</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"foreground"} -->
<p class="has-foreground-color has-text-color">CEO , Founder</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"style":{"spacing":{"padding":{"bottom":"15px","left":"30px"}},"border":{"width":"0px","style":"none"}},"className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark" style="border-style:none;border-width:0px;padding-bottom:15px;padding-left:30px"><!-- wp:image {"align":"left","id":349,"sizeSlug":"full","linkDestination":"none","style":{"border":{"radius":"10px","width":"10px"}},"borderColor":"primary"} -->
<figure class="wp-block-image alignleft size-full has-custom-border"><img src="https://demo.sparklewpthemes.com/spiderprime/wp-content/uploads/2023/02/testi_3_2.jpg" alt="" class="has-border-color has-primary-border-color wp-image-349" style="border-width:10px;border-radius:10px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"},"blockGap":"10px"}},"backgroundColor":"white","className":"has-shadow-dark  animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide has-shadow-dark animated animated-fadeInUp has-white-background-color has-background" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:30px;padding-bottom:30px"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground"} -->
<p class="has-text-align-left has-foreground-color has-text-color" style="font-style:normal;font-weight:300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
<!-- /wp:paragraph -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"textColor":"foreground","fontSize":"content-heading"} -->
<h2 class="has-foreground-color has-text-color has-content-heading-font-size">David Fernandes</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"foreground"} -->
<p class="has-foreground-color has-text-color">CEO , Founder</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->