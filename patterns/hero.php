<?php
 /**
  * Title: Hero #2
  * Slug: construction-renovationx/hero
  * Categories: construction-renovationx
  * Keywords: hero, banner, section
  */
?>

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-group alignfull" id="hero" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:cover {"url":"<?php echo esc_url(get_stylesheet_directory_uri(). '/images/wood-house-floor-home-ceiling-construction-633136-pxhere.com.jpg'); ?>","id":6,"dimRatio":20,"overlayColor":"foreground","isDark":false,"align":"full","style":{"spacing":{"padding":{"top":"9vw","bottom":"14vw","left":"30px"}}}} -->
<div class="wp-block-cover alignfull is-light" style="padding-top:9vw;padding-bottom:14vw;padding-left:30px"><span aria-hidden="true" class="wp-block-cover__background has-foreground-background-color has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-6" alt="" src="<?php echo esc_url(get_stylesheet_directory_uri(). '/images/wood-house-floor-home-ceiling-construction-633136-pxhere.com.jpg'); ?>" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"0px","padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|20"}},"layout":{"type":"flex","flexWrap":"wrap"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","level":6,"style":{"typography":{"lineHeight":"1.5","textTransform":"none"}},"textColor":"background","className":" animated animated-fadeInUp","fontFamily":"oxygen"} -->
<h6 class="has-text-align-left animated animated-fadeInUp has-background-color has-text-color has-oxygen-font-family" style="line-height:1.5;text-transform:none">Trusted by more than&nbsp;</h6>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"left","level":6,"style":{"typography":{"lineHeight":"1.5","textTransform":"none"}},"textColor":"primary","className":" animated animated-fadeInUp","fontFamily":"oxygen"} -->
<h6 class="has-text-align-left animated animated-fadeInUp has-primary-color has-text-color has-oxygen-font-family" style="line-height:1.5;text-transform:none">3 Million&nbsp;</h6>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"left","level":6,"style":{"typography":{"lineHeight":"1.5","textTransform":"none"}},"textColor":"background","className":" animated animated-fadeInUp","fontFamily":"oxygen"} -->
<h6 class="has-text-align-left animated animated-fadeInUp has-background-color has-text-color has-oxygen-font-family" style="line-height:1.5;text-transform:none">customers.</h6>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0","margin":{"bottom":"var:preset|spacing|60"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="margin-bottom:var(--wp--preset--spacing--60)"><!-- wp:heading {"textAlign":"left","style":{"typography":{"lineHeight":"1.5","letterSpacing":"3px"}},"textColor":"background","className":"animated animated-fadeInUp","fontSize":"huge"} -->
<h2 class="has-text-align-left animated animated-fadeInUp has-background-color has-text-color has-huge-font-size" style="letter-spacing:3px;line-height:1.5">Clean Home</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|20"}},"layout":{"type":"flex","flexWrap":"wrap"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"left","style":{"typography":{"lineHeight":"1.5","letterSpacing":"3px"}},"textColor":"background","className":"animated animated-fadeInUp","fontSize":"huge"} -->
<h2 class="has-text-align-left animated animated-fadeInUp has-background-color has-text-color has-huge-font-size" style="letter-spacing:3px;line-height:1.5">Decoration</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"left","style":{"typography":{"lineHeight":"1.5","letterSpacing":"3px"}},"textColor":"primary","className":"animated animated-fadeInUp","fontSize":"huge"} -->
<h2 class="has-text-align-left animated animated-fadeInUp has-primary-color has-text-color has-huge-font-size" style="letter-spacing:3px;line-height:1.5">Services</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|20","padding":{"top":"var:preset|spacing|40"}}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color" style="padding-top:var(--wp--preset--spacing--40)"><!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|20"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"className":"dashicons dashicons-saved"} -->
<p class="dashicons dashicons-saved"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>5 Year Warranty on Workmanship</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|20"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"className":"dashicons dashicons-saved"} -->
<p class="dashicons dashicons-saved"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Every Project is Run by a Team of Experts</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|20"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"className":"dashicons dashicons-saved"} -->
<p class="dashicons dashicons-saved"></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We Save You Time, Money, and Space</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"primary","textColor":"background"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-background-color has-primary-background-color has-text-color has-background wp-element-button" href="https://demo.sparkletheme.com/hello-fse/renovation/contact/">Contact Us</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}}}} -->
<div class="wp-block-column" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->