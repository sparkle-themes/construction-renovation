<?php
 /**
  * Title: Footer
  * Slug: construction-renovationx/footer
  * Categories: construction-renovationx
  * Keywords: footer, section
  */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}}},"backgroundColor":"foreground","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-foreground-background-color has-background" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:cover {"url":"https://demo.sparkletheme.com/hello-fse/renovation/wp-content/uploads/sites/5/2023/03/counter_bg_1.png","id":142,"isRepeated":true,"dimRatio":0,"focalPoint":{"x":0.49,"y":0.61},"isDark":false,"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|60","right":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60"}}}} -->
<div class="wp-block-cover alignfull is-light is-repeated" style="padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><div role="img" class="wp-block-cover__image-background wp-image-142 is-repeated" style="background-position:49% 61%;background-image:url(https://demo.sparkletheme.com/hello-fse/renovation/wp-content/uploads/sites/5/2023/03/counter_bg_1.png)"></div><div class="wp-block-cover__inner-container"><!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"2rem","left":"0","right":"0","bottom":"0rem"},"blockGap":"0px"},"elements":{"link":{"color":{"text":"var:preset|color|foreground"}}}},"textColor":"background","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-background-color has-text-color has-link-color" style="padding-top:2rem;padding-right:0;padding-bottom:0rem;padding-left:0"><!-- wp:group {"align":"wide","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"align":"wide","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:social-links {"showLabels":true,"size":"has-large-icon-size","align":"left","layout":{"type":"flex","justifyContent":"left"}} -->
<ul class="wp-block-social-links alignleft has-large-icon-size has-visible-labels"><!-- wp:social-link {"url":"#","service":"facebook"} /-->

<!-- wp:social-link {"url":"#","service":"instagram"} /-->

<!-- wp:social-link {"url":"#","service":"twitter"} /-->

<!-- wp:social-link {"url":"#","service":"tiktok"} /--></ul>
<!-- /wp:social-links --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"bottom":"3rem","top":"3rem"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:3rem;padding-bottom:3rem"><!-- wp:column {"width":"45%","style":{"spacing":{"padding":{"top":"20px","right":"20px","bottom":"20px","left":"0px"},"blockGap":"10px"}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-column animated animated-fadeInUp" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:0px;flex-basis:45%"><!-- wp:group {"style":{"spacing":{"blockGap":"6px"}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:heading {"level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500","fontSize":"1.5rem"}},"textColor":"white","className":"sp-underline"} -->
<h3 class="sp-underline has-white-color has-text-color" style="font-size:1.5rem;font-style:normal;font-weight:500">ABOUT US</h3>
<!-- /wp:heading -->

<!-- wp:spacer {"height":"13px"} -->
<div style="height:13px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:paragraph -->
<p>Enthusiastically reconceptualize seamless expertise vis-a-vis wireless bandwidth. Proactively benchmark frictionless leadership skills</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"13px"} -->
<div style="height:13px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:paragraph -->
<p><strong>Address: 18013 Northridge, CA 91325, United States</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Email: info.example@dalky.com</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Phone: +(1) 098 765 4321</strong> / <strong>+(1) 097 455 4321</strong>  </p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"13px"} -->
<div style="height:13px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:image {"id":156,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="https://demo.sparkletheme.com/hello-fse/renovation/wp-content/themes/hello-fse/images/payment-card.png" alt="" class="wp-image-156"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"","style":{"spacing":{"blockGap":"10px","padding":{"top":"20px"}}}} -->
<div class="wp-block-column" style="padding-top:20px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":"1.5rem","fontStyle":"normal","fontWeight":"500"}},"textColor":"white","className":"sp-underline"} -->
<h3 class="sp-underline has-white-color has-text-color" style="font-size:1.5rem;font-style:normal;font-weight:500">Important Link</h3>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|background"}}}}} -->
<ul class="has-link-color"><!-- wp:list-item -->
<li><a href="#">Our Services</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Contact Us</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">FAQ</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Team Members</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Privacy &amp; Policy</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Term &amp; Conditions</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Quick Support</a></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"","style":{"spacing":{"padding":{"top":"20px"},"blockGap":"10px"}}} -->
<div class="wp-block-column" style="padding-top:20px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:heading {"level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500","fontSize":"1.5rem"}},"textColor":"white","className":"sp-underline"} -->
<h3 class="sp-underline has-white-color has-text-color" style="font-size:1.5rem;font-style:normal;font-weight:500">Terms &amp; Conditions</h3>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|background"}}}}} -->
<ul class="has-link-color"><!-- wp:list-item -->
<li><a href="#">Our Services</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Contact Us</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">FAQ</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Team Members</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Privacy &amp; Policy</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Term &amp; Conditions</a></li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li><a href="#">Quick Support</a></li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->